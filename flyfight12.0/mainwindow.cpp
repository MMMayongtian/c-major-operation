#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),turns(true),run(true),game_run(true)
{
    void paintEvent(QPaintEvent*event);


    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent *event)
{

    if(run)
    {
        init();
    }
    setFixedSize(900,800);

    QPainter painter(this);

    QFont font1("Courier", 17);
    painter.setFont(font1);
    painter.setPen(Qt::black);
    painter.setBrush(Qt::red);
    painter.drawText(650, 60, "Score");
    painter.drawText(750, 60, QString::number(Score));

    QFont font2("Courier", 17);
    painter.setFont(font1);
    painter.setPen(Qt::black);
    painter.setBrush(Qt::red);
    painter.drawText(650, 260, "点击右侧空白区域复活");



    backpic=new QPixmap(":/pictures/img/b1.png");
    bak=new QPixmap(":/pictures/img/b2.png");
    painter.drawPixmap(Background1,*backpic);
    painter.drawPixmap(Background2,*bak);

    flypic=new QPixmap(":/pictures/img/fly.png");
    painter.drawPixmap(fly,*flypic);

    enemypic[0]=new QPixmap(":/pictures/img/enemy.png");
    enemypic[1]=new QPixmap(":/pictures/img/enemy2.png");
    enemypic[2]=new QPixmap(":/pictures/img/enemy3.png");

    for(int i=0;i<12;i++)
    {
        painter.drawPixmap(enemys[i].enemy,*enemypic[i%3]);
    }

    bulletpic=new QPixmap(":/pictures/img/bullet_11.png");
    for(int i=0;i<NUM_B;i++)
    {
        //右子弹
        painter.drawPixmap(bulletsR[i].bullet,*bulletpic);

        painter.drawPixmap(bullets[i].bullet,*bulletpic);
    }


    bombpic[0]=new QPixmap(":/pictures/img/1.png");
    bombpic[1]=new QPixmap(":/pictures/img/3 (2).png");
    bombpic[2]=new QPixmap(":/pictures/img/3 (1).png");
    bombpic[3]=new QPixmap(":/pictures/img/4.png");
    bombpic[4]=new QPixmap(":/pictures/img/bomb-3.png");
    bombpic[5]=new QPixmap(":/pictures/img/bomb-4.png");
    bombpic[6]=new QPixmap(":/pictures/img/bomb-5.png");
    bombpic[7]=new QPixmap(":/pictures/img/bomb-6.png");
    bombpic[8]=new QPixmap(":/pictures/img/bomb-7.png");
    for(int i=0;i<30;i++)
    {
        painter.drawPixmap(booms[i].boom,*bombpic[booms[i].times]);
    }
}



void MainWindow::init()
{
    QSound::play(":/Sounds/sound/bg.wav");
    run=false;
    QRect rect1(0, 0, 640, 800);
    Background1=rect1;
    QRect rect2(0,-800,640,800);
    Background2=rect2;
    QRect rect3(250,700,150,100);
    fly=rect3;

    timer = new QTimer(this);//设定计时器时间
    timer->start(10);
    connect(timer,SIGNAL(timeout()),SLOT(Backgroundupdate()));


    timerbullet = new QTimer(this);//设定计时器时间
    timerbullet->start(5);
    connect(timerbullet,SIGNAL(timeout()),SLOT(bullet_update()));

    timershoot=new QTimer(this);
    timershoot->start(120);
    connect(timershoot,SIGNAL(timeout()),SLOT(shoot()));

    timerEnemy = new QTimer(this);//设定计时器时间
    timerEnemy->start(4);
    connect(timerEnemy,SIGNAL(timeout()),SLOT(enemy_update()));

    timerEnBegin=new QTimer(this);
    timerEnBegin->start(250);
    connect(timerEnBegin,SIGNAL(timeout()),SLOT(enemy_begin()));

    exam=new QTimer(this);
    exam->start(1);
    connect(exam,SIGNAL(timeout()),SLOT(kill()));

    bombtimer =new QTimer(this);
    bombtimer->start(45);
    connect(bombtimer,SIGNAL(timeout()),SLOT(enemy_boom()));
}
Bullets::Bullets()
{
   bullet_use=true;
   QRect rect(-100,-100,60,100);
   bullet=rect;
}
Enemys::Enemys()
{
    enemys_use=true;
    QRect rect(-150,-100,150,100);
    enemy=rect;
}
Booms::Booms()
{
    times=0;
    boom_use=true;
    QRect rect(-100,-100,50,50);
    boom=rect;
}
void MainWindow::shoot()
{
    if(!game_run)
    {
        return;
    }
    for(int i=0;i<NUM_B;i++)
    {
        if(bullets[i].bullet_use&&bulletsR[i].bullet_use)
        {
            //右子弹

            QSound::play(":/Sounds/sound/2471.wav");
            bulletsR[i].bullet.setTop(fly.top()-50);
            bulletsR[i].bullet.setBottom(fly.top()+50);
            bulletsR[i].bullet.setLeft(fly.left()+105);
            bulletsR[i].bullet.setRight(fly.left()+155);
            bulletsR[i].bullet_use=false;

            bullets[i].bullet.setTop(fly.top()-50);
            bullets[i].bullet.setBottom(fly.top()+50);
            bullets[i].bullet.setLeft(fly.left()+0);
            bullets[i].bullet.setRight(fly.left()+50);
            bullets[i].bullet_use=false;
            return;
        }
    }
}
void MainWindow::bullet_update()
{
    for(int i=0;i<NUM_B;i++)
    {
        if(!bullets[i].bullet_use)
        {
            bullets[i].bullet.setTop(bullets[i].bullet.top()-4);
            bullets[i].bullet.setBottom(bullets[i].bullet.bottom()-4);
        }
        if(!bulletsR[i].bullet_use)
        {
            //右子弹
            bulletsR[i].bullet.setTop(bulletsR[i].bullet.top()-4);
            bulletsR[i].bullet.setBottom(bulletsR[i].bullet.bottom()-4);

        }
        if(bullets[i].bullet.bottom()<0)
        {
            bullets[i].bullet_use=true;
        }
        if(bulletsR[i].bullet.bottom()<0)
        {
            //右子弹
            bulletsR[i].bullet_use=true;
        }
    }

    update();
}

void MainWindow::enemy_begin()
{
    int s=0;
    for(int i=0;i<NUM_E;i++)
    {
        if(enemys[i].enemys_use)
        {
            int x=rand()%(50);
        enemys[i].enemy.setTop(0);
        enemys[i].enemy.setBottom(100);
        enemys[i].enemy.setLeft(10*x);
        enemys[i].enemy.setRight(150+10*x);
        enemys[i].enemys_use=false;
        s++;
        if(s==2)
        {
        return;
        }
        }
    }
}
void MainWindow::enemy_update()
{
    for(int i=0;i<NUM_E;i++)
    {
        if(!enemys[i].enemys_use)
        {
            enemys[i].enemy.setTop(enemys[i].enemy.top()+4);
            enemys[i].enemy.setBottom(enemys[i].enemy.bottom()+4);
        }

        if(enemys[i].enemy.top()>800)
        {
            enemys[i].enemys_use=true;
        }
    }

    update();

}

void MainWindow::isboom(QRect enemy)
{
    for(int i=0;i<30;i++)
    {
        if(booms[i].boom_use)
        {
            booms[i].boom.setTop(enemy.top()+20);
            booms[i].boom.setBottom(enemy.bottom());
            booms[i].boom.setLeft(enemy.left()+35);
            booms[i].boom.setRight(enemy.right()-35);
            booms[i].times=0;
            booms[i].boom_use=false;
            return;
        }
    }
}
void MainWindow::enemy_boom()
{
    for(int i=0;i<30;i++)
    {
        if(!booms[i].boom_use&&booms[i].times<=7)
        {
        booms[i].times++;
       // booms[i].boom.setTop(booms[i].boom.top()+booms[i].times*0);
       // booms[i].boom.setBottom(booms[i].boom.bottom()+booms[i].times*0);
        }
        else if(!booms[i].boom_use&&booms[i].times>7)
        {
            booms[i].boom_use=true;
            QRect rect(-100,-100,80,80);
            booms[i].boom=rect;
        }
    }
}

void MainWindow::kill()
{
    if(!game_run)
    {
        return;
    }
    for(int i=0;i<NUM_E;i++)
    {
        if(enemys[i].enemys_use)
        {
            continue;
        }
        for(int j=0;j<NUM_B;j++)
        {
            bool iboom=0;
            QRect enemyss=enemys[i].enemy;
            if(enemys[i].enemy.intersects(bullets[j].bullet)&&bullets[j].bullet.top()>=80)
            {
                enemys[i].enemys_use=true;
                enemys[i].enemy.setTop(-1);
                enemys[i].enemy.setBottom(-1);

                bullets[j].bullet_use=true;
                bullets[j].bullet.setTop(-1);
                bullets[j].bullet.setBottom(-1);

                iboom=1;
            }
            else if(enemys[i].enemy.intersects(bulletsR[j].bullet)&&bulletsR[j].bullet.top()>=80)
            {
                enemys[i].enemys_use=true;
                enemys[i].enemy.setTop(-1);
                enemys[i].enemy.setBottom(-1);

                bulletsR[j].bullet_use=true;
                bulletsR[j].bullet.setTop(-1);
                bulletsR[j].bullet.setBottom(-1);

                iboom=1;
            }
             if(iboom)
             {
                 Score++;

                 isboom(enemyss);

             }

        }
        if(enemys[i].enemy.intersects(fly)&&!enemys[i].enemys_use)
        {
            isboom(fly);
            // QSound::play(":/Sounds/sound/12439.wav");

             fly.setTop(-150);
             fly.setBottom(-150);
             game_run=false;

             QMessageBox::information(this,"RESULT","GAME OVER!!! Your score is "+QString::number(this->Score));
             Score=0;
        }
    }
}
void MainWindow::Backgroundupdate()
{

    Background1.setTop(Background1.top()+4);
    Background1.setBottom(Background1.bottom()+4);
    Background2.setTop(Background2.top()+4);
    Background2.setBottom(Background2.bottom()+4);
 if(turns)
 {
    if(Background1.top()>=800)
    {
        Background1.setTop(-800);
        Background1.setBottom(0);
        turns=false;
    }
 }
 else
 {
     if(Background2.top()>=800)
    {
        Background2.setTop(-800);
        Background2.setBottom(0);
        turns=true;
     }
 }
 update();
}
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    QKeyEvent *key=(QKeyEvent*)event;
    switch (key->key())
    {
    case Qt::Key_Up:
        nDrection=1;
        break;
    case Qt::Key_Down:
        nDrection=2;
        break;
    case Qt::Key_Left:
        nDrection=3;
        break;
    case Qt::Key_Right:
        nDrection=4;
        break;
    }
   fly_update();
}
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if(!game_run&&event->x()<=640)
    {
        return;
    }
    else if(event->x()>640)
    {
       if(!game_run)
        {
            for(int i=0;i<NUM_E;i++)
            {
              //QSound::play(":/Sounds/sound/12439.wav");
               isboom(enemys[i].enemy);
               enemys[i].enemys_use=true;
               enemys[i].enemy.setTop(-1);
               enemys[i].enemy.setBottom(-1);
            }
            fly.setTop(700);
            fly.setBottom(800);
            fly.setLeft(300);
            fly.setRight(450);

            game_run=true;
       }
       else
       {
           for(int i=0;i<NUM_E;i++)
           {
             //QSound::play(":/Sounds/sound/12439.wav");
             isboom(enemys[i].enemy);
              enemys[i].enemys_use=true;
              enemys[i].enemy.setTop(0);
              enemys[i].enemy.setBottom(0);
           }
       }
    }
    int x=event->x()-fly.width()*0.5;
    int y=event->y()-fly.height()*0.5;
    if(x>=0&&x<=490&&y>=0&&y<=700)
    {

    fly.setLeft(x);
    fly.setRight(x+150);
    fly.setTop(y);
    fly.setBottom(y+100);
    }
  // if(y>=0&&y<=700)
  //  {
   // }
}
void MainWindow::fly_update()
{
    if(!game_run)
    {
        return;
    }
    switch (nDrection) {
    case 1:
        if(fly.top()>=10)
        {
        fly.setTop(fly.top()-13);
        fly.setBottom(fly.bottom()-13);
        }
        break;
    case 2:
        if(fly.bottom()<=800)
        {
        fly.setTop(fly.top()+13);
        fly.setBottom(fly.bottom()+13);
        }
        break;
    case 3:
        if(fly.left()>=10)
        {
        fly.setLeft(fly.left()-15);
        fly.setRight(fly.right()-15);
        }
        break;
    case 4:
        if(fly.right()<=630)
        {
        fly.setLeft(fly.left()+15);
        fly.setRight(fly.right()+15);
        }
        break;
    default:;
    }
      update();//更新刷屏
}
