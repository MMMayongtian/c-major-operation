#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#define NUM_B 6
#define NUM_E 8
#include <QMainWindow>
#include<QRect>
#include<QPainter>
#include<QTimer>
#include<time.h>
#include<QKeyEvent>
#include<QMouseEvent>
#include<QSound>
#include<QPushButton>
#include<QMessageBox>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
class Bullets
{
public:
    Bullets();
    QRect bullet;
    bool bullet_use;
};
class Enemys
{
public:
    Enemys();
    QRect enemy;
    bool enemys_use;
};
class Booms
{
public:
    Booms();
    QRect boom;
    int times=0;
    bool boom_use=true;
};
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void paintEvent(QPaintEvent*event);
    void keyPressEvent(QKeyEvent*event);
    void mouseMoveEvent(QMouseEvent*event);
    void fly_update();
    void init();


private slots:

    void isboom(QRect enemy);
    void enemy_boom();

    void kill();

    void shoot();
    void bullet_update();

    void enemy_begin();
    void enemy_update();

    void Backgroundupdate();

private:

    QPushButton *Start;
    QRect Background1;
    QRect Background2;
    QRect fly;

    Bullets bullets[8];
    Bullets bulletsR[8];

    Enemys enemys[12];

    Booms booms[30];
    QPixmap *bombpic[10];
    QTimer *bombtimer;

    QPixmap *bulletpic;
    QPixmap *flypic;
    QPixmap *backpic;
    QPixmap *bak;
    QPixmap *enemypic[3];

    QTimer *timer;

    QTimer *exam;

    QTimer *timerbullet;
    QTimer *timershoot;

    QTimer *timerEnBegin;
    QTimer *timerEnemy;
    int nDrection;
    int Score=0;

    bool game_run;
    bool run;
    bool turns;

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
