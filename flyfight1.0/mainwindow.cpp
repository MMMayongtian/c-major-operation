#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),run(true)
{
    void paintEvent(QPaintEvent*event);
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent *event)
{
    if(run)
    {
        init();
    }
    setFixedSize(800,800);
    QPainter painter(this);

    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);
    painter.drawRect(Background);

    painter.setPen(Qt::black);
    painter.setBrush(Qt::blue);
    painter.drawRect(fly);

    painter.setPen(Qt::black);
    painter.setBrush(Qt::red);
    painter.drawRect(enemy);

}
void MainWindow::init()
{
    run=false;
    QRect rect(0, 0, 550, 800);
    Background=rect;
    QRect rect1(200,650,150,100);
    fly=rect1;
    QRect rect2(200,20,100,70);
    enemy=rect2;
}

