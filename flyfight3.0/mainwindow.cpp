#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),turns(true),run(true)
{
    void paintEvent(QPaintEvent*event);
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent *event)
{
    if(run)
    {
        init();
    }
    setFixedSize(800,800);
    QPainter painter(this);
    backpic=new QPixmap(":/pictures/img/back1.jpg");
    painter.drawPixmap(Background1,*backpic);
    painter.drawPixmap(Background2,*backpic);

   //painter.setPen(Qt::black);
   //painter.setBrush(Qt::blue);
   //painter.drawRect(fly);
    flypic=new QPixmap(":/pictures/img/fly.png");
    painter.drawPixmap(fly,*flypic);


   // painter.setPen(Qt::black);
   // painter.setBrush(Qt::red);
   // painter.drawRect(enemy);
    enemypic=new QPixmap(":/pictures/img/enemy.png");
    painter.drawPixmap(enemy,*enemypic);
}
void MainWindow::init()
{
    run=false;
    QRect rect1(0, 0, 550, 800);
    Background1=rect1;
    QRect rect2(0,-800,550,800);
    Background2=rect2;
    QRect rect3(200,650,150,100);
    fly=rect3;
    QRect rect4(200,20,150,100);
    enemy=rect4;

    timer = new QTimer(this);//设定计时器时间
    timer->start(5);
    connect(timer,SIGNAL(timeout()),SLOT(Backgroundupdate()));

}
void MainWindow::Backgroundupdate()
{
    Background1.setTop(Background1.top()+5);
    Background1.setBottom(Background1.bottom()+5);
    Background2.setTop(Background2.top()+5);
    Background2.setBottom(Background2.bottom()+5);
 if(turns)
 {
    if(Background1.top()>=800)
    {
        Background1.setTop(-800);
        Background1.setBottom(0);
        turns=false;
    }
 }
 else
 {
     if(Background2.top()>=800)
    {
        Background2.setTop(-800);
        Background2.setBottom(0);
        turns=true;
     }
 }

 update();

}
