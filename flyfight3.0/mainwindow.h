#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include<QRect>
#include<QPainter>
#include<QTimer>
#include<QPixmap>
#include<time.h>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void paintEvent(QPaintEvent*event);
    void init();


private slots:

    void Backgroundupdate();


private:
    QRect Background1;
    QRect Background2;
    QRect fly;
    QRect enemy;

    QPixmap *flypic;
    QPixmap *enemypic;
    QPixmap *backpic;

    QTimer *timer;
    bool run;
    bool turns;

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
