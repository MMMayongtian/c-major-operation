#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),turns(true),run(true)
{
    void paintEvent(QPaintEvent*event);
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent *event)
{
    if(run)
    {
        init();
    }
    setFixedSize(900,800);

    QPainter painter(this);

    backpic=new QPixmap(":/pictures/img/back1.jpg");
    painter.drawPixmap(Background1,*backpic);
    painter.drawPixmap(Background2,*backpic);


    //painter.setPen(Qt::black);
    //painter.setBrush(Qt::blue);
    //painter.drawRect(fly);
    flypic=new QPixmap(":/pictures/img/fly.png");
    painter.drawPixmap(fly,*flypic);


    //painter.setPen(Qt::black);
    //painter.setBrush(Qt::red);
    //painter.drawRect(enemy);
    enemypic=new QPixmap(":/pictures/img/enemy.png");
    //painter.drawPixmap(enemy,*enemypic);
    for(int i=0;i<10;i++)
    {
        painter.drawPixmap(enemys[i].enemy,*enemypic);

    }



   //painter.setPen(Qt::black);
   //painter.setBrush(Qt::red);
    bulletpic=new QPixmap(":/pictures/img/bullet_11.png");
    for(int i=0;i<8;i++)
    {
        //右子弹
        painter.drawPixmap(bulletsR[i].bullet,*bulletpic);

        painter.drawPixmap(bullets[i].bullet,*bulletpic);
    }



}
void MainWindow::init()
{
    run=false;
    QRect rect1(0, 0, 640, 800);
    Background1=rect1;
    QRect rect2(0,-800,640,800);
    Background2=rect2;
    QRect rect3(200,650,150,100);
    fly=rect3;
   // QRect rect4(200,20,150,100);
   // enemy=rect4;

    timer = new QTimer(this);//设定计时器时间
    timer->start(5);
    connect(timer,SIGNAL(timeout()),SLOT(Backgroundupdate()));


    timerbullet = new QTimer(this);//设定计时器时间
    timerbullet->start(3);
    connect(timerbullet,SIGNAL(timeout()),SLOT(bullet_update()));

    timershoot=new QTimer(this);
    timershoot->start(90);
    connect(timershoot,SIGNAL(timeout()),SLOT(shoot()));


    timerEnemy = new QTimer(this);//设定计时器时间
    timerEnemy->start(3);
    connect(timerEnemy,SIGNAL(timeout()),SLOT(enemy_update()));

    timerEnBegin=new QTimer(this);
    timerEnBegin->start(150);
    connect(timerEnBegin,SIGNAL(timeout()),SLOT(enemy_begin()));

    exam=new QTimer(this);
    exam->start(1);
    connect(exam,SIGNAL(timeout()),SLOT(kill()));


}
Bullets::Bullets()
{
   bullet_use=true;
   QRect rect(-40,-40,30,50);
   bullet=rect;
}
Enemys::Enemys()
{
    enemys_use=true;
    QRect rect(-150,-100,150,100);
    enemy=rect;
}
void MainWindow::shoot()
{
    for(int i=0;i<8;i++)
    {
        if(bullets[i].bullet_use&&bulletsR[i].bullet_use)
        {
            //右子弹
            bulletsR[i].bullet.setTop(fly.top());
            bulletsR[i].bullet.setBottom(fly.top()+50);
            bulletsR[i].bullet.setLeft(fly.left()+120);
            bulletsR[i].bullet.setRight(fly.left()+150);
            bulletsR[i].bullet_use=false;

            bullets[i].bullet.setTop(fly.top());
            bullets[i].bullet.setBottom(fly.top()+50);
            bullets[i].bullet.setLeft(fly.left()+0);
            bullets[i].bullet.setRight(fly.left()+30);
            bullets[i].bullet_use=false;
            return;
        }


    }
}
void MainWindow::enemy_begin()
{
    for(int i=0;i<10;i++)
    {
        if(enemys[i].enemys_use)
        {
        enemys[i].enemy.setTop(0);
        enemys[i].enemy.setBottom(100);
        enemys[i].enemy.setLeft(0+i*80);
        enemys[i].enemy.setRight(150+i*80);
        enemys[i].enemy.setLeft(0);
        enemys[i].enemy.setRight(150);
        enemys[i].enemys_use=false;
        return;
        }
    }
}
void MainWindow::enemy_update()
{
    for(int i=0;i<10;i++)
    {
        if(!enemys[i].enemys_use)
        {
            enemys[i].enemy.setTop(enemys[i].enemy.top()+1);
            enemys[i].enemy.setBottom(enemys[i].enemy.bottom()+1);
        }

        if(enemys[i].enemy.top()>800)
        {
            enemys[i].enemys_use=true;
        }

    }

    update();

}
void MainWindow::bullet_update()
{
    for(int i=0;i<8;i++)
    {
        if(!bullets[i].bullet_use)
        {

            bullets[i].bullet.setTop(bullets[i].bullet.top()-2);
            bullets[i].bullet.setBottom(bullets[i].bullet.bottom()-2);
        }
        if(!bulletsR[i].bullet_use)
        {
            //右子弹
            bulletsR[i].bullet.setTop(bulletsR[i].bullet.top()-2);
            bulletsR[i].bullet.setBottom(bulletsR[i].bullet.bottom()-2);

        }
        if(bullets[i].bullet.bottom()<0)
        {

            bullets[i].bullet_use=true;

        }
        if(bulletsR[i].bullet.bottom()<0)
        {
            //右子弹
            bulletsR[i].bullet_use=true;
        }
    }

    update();

}

void MainWindow::kill()
{
    for(int i=0;i<10;i++)
    {
        if(enemys[i].enemys_use)
        {
            continue;
        }
        for(int j=0;j<8;j++)
        {
            if(bullets[i].bullet_use)
            {
                continue;
            }
            if(enemys[i].enemy.intersects(bullets[j].bullet))
            {
                enemys[i].enemys_use=true;
                enemys[i].enemy.setTop(810);
                enemys[i].enemy.setBottom(910);

                bullets[j].bullet_use=true;
                bullets[j].bullet.setTop(810);
                bullets[j].bullet.setBottom(860);
            }
            else if(enemys[i].enemy.intersects(bulletsR[j].bullet))
               {
                enemys[i].enemys_use=true;
                enemys[i].enemy.setTop(810);
                enemys[i].enemy.setBottom(910);

                bulletsR[j].bullet_use=true;
                bulletsR[j].bullet.setTop(810);
                bulletsR[j].bullet.setBottom(860);
            }
        }

    }


}
void MainWindow::Backgroundupdate()
{
    Background1.setTop(Background1.top()+5);
    Background1.setBottom(Background1.bottom()+5);
    Background2.setTop(Background2.top()+5);
    Background2.setBottom(Background2.bottom()+5);
 if(turns)
 {
    if(Background1.top()>=800)
    {
        Background1.setTop(-800);
        Background1.setBottom(0);
        turns=false;
    }
 }
 else
 {
     if(Background2.top()>=800)
    {
        Background2.setTop(-800);
        Background2.setBottom(0);
        turns=true;
     }
 }
 update();
}
void MainWindow::keyPressEvent(QKeyEvent *event)
{
    QKeyEvent *key=(QKeyEvent*)event;
    switch (key->key())
    {
    case Qt::Key_Up:
        nDrection=1;
        break;
    case Qt::Key_Down:
        nDrection=2;
        break;
    case Qt::Key_Left:
        nDrection=3;
        break;
    case Qt::Key_Right:
        nDrection=4;
        break;
    }
   fly_update();
}
void MainWindow::mouseMoveEvent(QMouseEvent *event)
{

    int x=event->x()-fly.width()*0.5;
    int y=event->y()-fly.height()*0.5;
    fly.setTop(y);
    fly.setBottom(y+100);
    fly.setLeft(x);
    fly.setRight(x+150);
}
void MainWindow::fly_update()
{
    switch (nDrection) {
    case 1:
        if(fly.top()>=10)
        {
        fly.setTop(fly.top()-13);
        fly.setBottom(fly.bottom()-13);
        }
        break;
    case 2:
        if(fly.bottom()<=800)
        {
        fly.setTop(fly.top()+13);
        fly.setBottom(fly.bottom()+13);
        }
        break;
    case 3:
        if(fly.left()>=10)
        {
        fly.setLeft(fly.left()-15);
        fly.setRight(fly.right()-15);
        }
        break;
    case 4:
        if(fly.right()<=630)
        {
        fly.setLeft(fly.left()+15);
        fly.setRight(fly.right()+15);
        }
        break;
    default:;
    }

      update();//更新刷屏
}
