#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include<QRect>
#include<QPainter>
#include<QTimer>
#include<time.h>
#include<QKeyEvent>
#include<QMouseEvent>
#include<QString>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
//QString bombpics[7]={":/pictures/img/bomb-1.png",":/pictures/img/bomb-2.png",":/pictures/img/bomb-3.png",":/pictures/img/bomb-4.png",":/pictures/img/bomb-5.png",":/pictures/img/bomb-6.png",":/pictures/img/bomb-7.png"};

class Bullets
{
public:
    Bullets();
    QRect bullet;
    bool bullet_use;
};
class Enemys
{
public:
    Enemys();
    QRect enemy;
    bool enemys_use;
};
class Booms
{
public:
    Booms();
    QRect boom;
    int times=0;
    bool boom_use=true;
};
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void paintEvent(QPaintEvent*event);
    void keyPressEvent(QKeyEvent*event);
    void mouseMoveEvent(QMouseEvent*event);
    void fly_update();
    void init();


private slots:

    void isboom(QRect enemy);
    void enemy_boom();

    void kill();

    void shoot();
    void bullet_update();

    void enemy_begin();
    void enemy_update();

    void Backgroundupdate();


private:



    QRect Background1;
    QRect Background2;
    QRect fly;
    //QRect enemy;

    Bullets bullets[7];
    Bullets bulletsR[7];

    Enemys enemys[10];

    Booms booms[50];
    QPixmap *bombpic[10];
    QTimer *bombtimer;

    QPixmap *bulletpic;
    QPixmap *flypic;
    QPixmap *backpic;
    QPixmap *enemypic;

    QTimer *timer;

    QTimer *exam;

    QTimer *timerbullet;
    QTimer *timershoot;

    QTimer *timerEnBegin;
    QTimer *timerEnemy;
    int nDrection;
    bool run;
    bool turns;

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
