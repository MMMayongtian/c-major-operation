#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include<QRect>
#include<QPainter>
#include<QPixmap>
#include<QTimer>
#include<time.h>
#include<QKeyEvent>
#include<QMouseEvent>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void paintEvent(QPaintEvent*event);
    void keyPressEvent(QKeyEvent*event);
    void mouseMoveEvent(QMouseEvent*event);
    void fly_update();
    void init();


private slots:

    void Backgroundupdate();


private:
    QRect Background1;
    QRect Background2;
    QRect fly;
    QRect enemy;

    QPixmap *flypic;
    QPixmap *backpic;
    QPixmap *enemypic;
    QTimer *timer;
    int nDrection;
    bool run=true;
    bool turns=true;

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
