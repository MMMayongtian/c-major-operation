#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include<QRect>
#include<QPainter>
#include<QTimer>
#include<time.h>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void paintEvent(QPaintEvent*event);
    void init();


private:
    QRect Background;
    QRect fly;
    QRect enemy;
    QPixmap *backpic;
    bool run;


private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
