#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow),run(true)
{
    void paintEvent(QPaintEvent*event);
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::paintEvent(QPaintEvent *event)
{
    if(run)
    {
        init();
    }
    setFixedSize(800,800);
    QPainter painter(this);


    //painter.setPen(Qt::black);
    //painter.setBrush(Qt::white);
    //painter.drawRect(Background);
    backpic =new QPixmap(":/pictures/img/back1.jpg");
    painter.drawPixmap(Background,*backpic);

    //painter.setPen(Qt::black);
    //painter.setBrush(Qt::blue);
    //painter.drawRect(fly);
    flypic=new QPixmap(":/pictures/img/fly.png");
    painter.drawPixmap(fly,*flypic);

    //painter.setPen(Qt::black);
    //painter.setBrush(Qt::red);
    //painter.drawRect(enemy);
    enemypic=new QPixmap(":/pictures/img/enemy.png");
    painter.drawPixmap(enemy,*enemypic);

}
void MainWindow::init()
{
    run=false;
    QRect rect(0, 0, 550, 800);
    Background=rect;
    QRect rect1(200,650,150,100);
    fly=rect1;
    QRect rect2(200,20,150,100);
    enemy=rect2;
}

