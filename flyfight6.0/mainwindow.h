#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include<QRect>
#include<QPainter>
#include<QTimer>
#include<time.h>
#include<QKeyEvent>
#include<QMouseEvent>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class Bullets
{
public:
    Bullets();
    QRect bullet;
    bool bullet_use;
};
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


    void paintEvent(QPaintEvent*event);
    void keyPressEvent(QKeyEvent*event);
    void mouseMoveEvent(QMouseEvent*event);
    void fly_update();
    void init();


private slots:

    void shoot();
    void bullet_update();
    void Backgroundupdate();


private:
    QRect Background1;
    QRect Background2;
    QRect fly;
    QRect enemy;

    Bullets bullets[10];
    Bullets bulletsR[10];

    QPixmap *bulletpic;
    QPixmap *flypic;
    QPixmap *backpic;
    QPixmap *enemypic;

    QTimer *timer;
    QTimer *timerbullet;
    QTimer *timershoot;
    int nDrection;
    bool run;
    bool turns;

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
